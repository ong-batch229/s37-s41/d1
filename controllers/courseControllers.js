const Course = require("../models/Course");

// Create new course
/*
	Steps: 
	1. Create a new Course object using the mongoose model
	2. Save the new Course to the database
*/

module.exports.addCourse = (data) => {

	if(data.isAdmin){
		// Creates a new variable "newCourse" and instantiate a new Course object
		// Uses the info from the request body to provide all necessary info
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		// saves the created object to our database
		return newCourse.save().then((course, error)=> {
			// course creation failed
			if (error){
				return false;
			// course creation success
			}else{
				return true;
			};
		});
	}
}

// retrieve all courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	});
}

// retrieve all active courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// retrieve specific course

module.exports.getCourse = (reqParams) => {
	console.log(reqParams);
	return Course.findById(reqParams._id).then(result => {
		return result;
	});
}

// update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req body
	2. Find and update the course using the course ID retrieved from the req params and the variable "updatedCourse"
*/
// info for updating will be coming from URL parameters and request body
module.exports.updateCourse = (reqParams, reqBody) => {
	// specify the fields of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		decription: reqBody.description,
		price: reqBody.price 
	};
	// findByIdUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		// course not updated
		if(error){
			return false
		// course updated successfully
		}else{
			return true
		}
	})
}
	
// archiving a course
module.exports.archiveCourse = (data) => {
	if(data.isAdmin == true){
		let updateActiveField = {
			isActive: false
		};
		return Course.findByIdAndUpdate(data.reqParams.courseId, updateActiveField).then((course, error) => {
			if(error){
				return false;
			}else{
				return true
			}
		})
	}else{
		return false
	}
};






	